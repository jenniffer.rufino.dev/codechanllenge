import { Bucket, calculateBuckets } from "../models/bucket";
import { Wall, doorDimensions, windowDimensions } from "../models/wall";

interface PaintCalculationResult {
  totalLiters: number;
  buckets: Bucket;
  message?: string;
}

export function calculatePaintNeeded(walls: Wall[]): PaintCalculationResult {
  const paintCoverage = 5;
  let totalArea = 0;

  for (const wall of walls) {
    const wallArea = wall.width * wall.height;

    if (wallArea < 1 || wallArea > 50) {
      return { totalLiters: 0, buckets: {}, message: "Cada parede deve ter entre 1 e 50 metros quadrados." };
    }

    const doorArea = wall.doors * doorDimensions.width * doorDimensions.height;
    const windowArea = wall.windows * windowDimensions.width * windowDimensions.height;

    if (doorArea + windowArea > wallArea * 0.5) {
      return { totalLiters: 0, buckets: {}, message: "A área das portas e janelas não pode exceder 50% da área da parede." };
    }

    if (wall.doors > 0 && wall.height <= (doorDimensions.height + 0.3)) {
      return { totalLiters: 0, buckets: {}, message: "A altura das paredes com portas deve ser pelo menos 30 cm maior que a altura da porta." };
    }

    totalArea += (wallArea - doorArea - windowArea);
  }

  const paintNeeded = totalArea / paintCoverage;
  return {
    totalLiters: paintNeeded,
    buckets: calculateBuckets(paintNeeded)
  };
}
