import { Request, Response } from "express";
import { Wall } from "../models/wall";
import { calculatePaintNeeded } from "../services/paintService";

export function calculatePaintRequest(req: Request, res: Response): void {
  try {
    const walls: Wall[] = req.body.walls;
    const result = calculatePaintNeeded(walls);
    res.json(result);
  } catch (error) {
    console.error('Erro no cálculo da tinta:', error);
    res.status(500).send('Erro ao calcular a quantidade de tinta necessária');
  }
}
