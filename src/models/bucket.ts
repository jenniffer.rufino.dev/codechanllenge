export interface Bucket {
    [key: string]: number;
}
  
export function calculateBuckets(paintNeeded: number): Bucket {
    const sizes: number[] = [18, 3.6, 2.5, 0.5];
    const buckets: Bucket = {};
    let remaining = paintNeeded;
  
    sizes.forEach(size => {
      buckets[`${size}L`] = Math.floor(remaining / size);
      remaining -= buckets[`${size}L`] * size;
    });
  
    if (remaining > 0) {
      buckets['0.5L'] = (buckets['0.5L'] || 0) + 1;
    }
  
    return buckets;
}
  