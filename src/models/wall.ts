export interface Wall {
    width: number;
    height: number;
    doors: number;
    windows: number;
}
  
export const doorDimensions = { width: 0.8, height: 1.9 };
export const windowDimensions = { width: 2.0, height: 1.2 };
  