import { Router } from "express";
import { calculatePaintRequest } from "../controllers/paintController";

const router = Router();

router.post('/calculate-paint', calculatePaintRequest);

export default router;
