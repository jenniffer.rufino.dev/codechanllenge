import express, { Express } from 'express';
import cors from 'cors';
import paintRoutes from './routes/paintRoutes';

const app: Express = express();

app.use(cors());
app.use(express.json());
app.use('/api', paintRoutes);

export default app;
