interface Wall {
    width: number;
    height: number;
    doors: number;
    windows: number;
}

interface Bucket {
    [key: string]: number;
}

const doorDimensions = { width: 0.8, height: 1.9 };
const windowDimensions = { width: 2.0, height: 1.2 }; 

export const calculatePaintNeeded = (walls: Wall[]): { totalLiters: number, buckets: Bucket, message?: string } => {
    const paintCoverage = 5;
    let totalArea = 0;

    for (const { width, height, doors, windows } of walls) {
        const wallArea = width * height;

        if (wallArea < 1 || wallArea > 50) {
            return { totalLiters: 0, buckets: {}, message: "Cada parede deve ter entre 1 e 50 metros quadrados." };
        }

        const doorArea = doors * doorDimensions.width * doorDimensions.height;
        const windowArea = windows * windowDimensions.width * windowDimensions.height;

        if (doorArea + windowArea > wallArea * 0.5) {
            return { totalLiters: 0, buckets: {}, message: "A área das portas e janelas não pode exceder 50% da área da parede." };
        }

        if (doors > 0 && height <= (doorDimensions.height + 0.3)) {
            return { totalLiters: 0, buckets: {}, message: "A altura das paredes com portas deve ser pelo menos 30 cm maior que a altura da porta." };
        }

        totalArea += (wallArea - doorArea - windowArea);
    }

    const paintNeeded = totalArea / paintCoverage;
    console.log("Chama", paintNeeded)
    return {
        totalLiters: paintNeeded,
        buckets: calculateBuckets(paintNeeded)
    };
};

function calculateBuckets(paintNeeded: number): Bucket {
    const sizes = [18, 3.6, 2.5, 0.5];
    const buckets: Bucket = {};
    let remaining = paintNeeded;

    sizes.forEach(size => {
        buckets[`${size}L`] = Math.floor(remaining / size);
        remaining -= buckets[`${size}L`] * size;
    });

    if (remaining > 0) {
        buckets['0.5L'] = (buckets['0.5L'] || 0) + 1;
    }

    return buckets;
}
