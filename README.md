# Calculator Backend

Este é um serviço backend desenvolvido com TypeScript e Express que calcula a quantidade de tinta necessária para pintar paredes, considerando a presença de portas e janelas.

## Pré-requisitos

Antes de começar, você precisará ter o Node.js instalado em sua máquina. Você pode baixá-lo em [Node.js](https://nodejs.org/).

## Configuração do Projeto

### Clone o Repositório

Primeiro, clone o repositório do projeto usando o seguinte comando:

git clone https://gitlab.com/jenniffer.rufino.dev/codechanllenge.git
cd calculatorbackend

### Instale as Dependências

Dentro do diretório do projeto, execute o seguinte comando para instalar todas as dependências necessárias:

npm install

### Compilação

Compile o projeto TypeScript para JavaScript usando:

npm run build

Isso criará uma pasta `dist` contendo os arquivos JavaScript compilados.

## Executando o Servidor

Para iniciar o servidor, execute:

npm start

Isso iniciará o servidor na porta 3001. Você pode acessar o servidor visitando `http://localhost:3001`.

## Desenvolvimento

Para desenvolvimento com recarga automática, utilize:

npm run dev

Este comando permite que você faça alterações no código TypeScript e veja as mudanças em tempo real sem recompilar manualmente.

## Testando a API

Para testar se a API está funcionando, envie uma requisição POST para `http://localhost:3001/api/calculate-paint` com um corpo JSON como este:

{
  "walls": [
    {
      "width": 4,
      "height": 3,
      "doors": 1,
      "windows": 1
    },
    {
      "width": 5,
      "height": 3,
      "doors": 0,
      "windows": 2
    }
  ]
}

A API deve retornar a quantidade total de tinta necessária e a distribuição por baldes.

## Licença

Este projeto está sob a licença ISC.
